<?php
    session_start();
    require('../database/Employeedb.php');

    $error=''; //Variable to store error message;
    if(isset($_POST['submit'])) {
        if(empty($_POST['user']) || empty($_POST['pass'])) {
            $error = "Username and Password is Invalid!!!";
        }
        else{
            //get $user and $pass
            $user = $_POST['user'];
            $pass = $_POST['pass'];
            
            $db = new Employeedb();
            $db->login($user, $pass);
        }
    }
?>