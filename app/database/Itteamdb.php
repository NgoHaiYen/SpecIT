<?php
    require('CalllogitDatabaseConnection.php');
    
    class Itteamdb{
        private $conn;

        function _construct(){

        }

        function getAll(){
            $db = new CalllogitDatabaseConnection();
            $this->conn = $db->connect();
            if (!$this->conn) {
                die("Connection failed: ".mysqli_connect_error());
            } else {
                $sql = "SELECT * FROM itteam";
                $result = mysqli_query($this->conn, $sql);
                if ($result->num_rows > 0) {
                    $i = 0;
                    while($row = $result->fetch_assoc()) {
                        $itteams[$i] = new Itteam($row['id'], $row['name']);
                        $i++;
                    }
                }
            }
            return $itteams;
        }

        public function getAllName(){
            $db = new CalllogitDatabaseConnection();
            $this->conn = $db->connect();
            $itname;
            if (!$this->conn) {
                die("Connection failed: ".mysqli_connect_error());
            } else {
                $sql = "SELECT name FROM itteam";
                $result = mysqli_query($this->conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    $i = 0;
                    while($row = mysqli_fetch_assoc($result)) {
                        $itname[$i] = $row['name'];
                        $i++;
                    }
                }
            }
            return $itname;
        }
    }
?>