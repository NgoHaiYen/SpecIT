<?php
    class CalllogitDatabaseConnection{
        private $conn;
        
        function __construct() {
            $this->conn = $this->connect();
        }

        public function connect(){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $databasename = "calllogit";

            $conn = new mysqli($servername, $username, $password, $databasename);
            mysqli_set_charset($conn,"utf8");
            return $conn;
        }

        public function close(){
            $this->conn->close();
        }
    }
?>