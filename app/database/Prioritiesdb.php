<?php
    require('CalllogitDatabaseConnection.php');
    
    class Prioritiesdb{
        private $conn;

        function _construct(){
            $db = new CalllogitDatabaseConnection();
        }

        function getAll(){
            $this->conn = $db->connect();
            if (!$this->conn) {
                die("Connection failed: ".mysqli_connect_error());
            } else {
                $sql = "SELECT * FROM priorities";
                $result = mysqli_query($this->conn, $sql);
                if ($result->num_rows > 0) {
                    $i = 0;
                    while($row = $result->fetch_assoc()) {
                        $priorities[$i] = new Priority($row['id'], $row['name']);
                        $i++;
                    }
                }
            }
            return $priorities;
        }

        public function getAllName(){
            $db = new CalllogitDatabaseConnection();
            $this->conn = $db->connect();
            $priorities;
            if (!$this->conn) {
                die("Connection failed: ".mysqli_connect_error());
            } else {
                $sql = "SELECT name FROM priorities";
                $result = mysqli_query($this->conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    $i = 0;
                    while($row = mysqli_fetch_assoc($result)) {
                        $priorities[$i] = $row['name'];
                        $i++;
                    }
                }
            }
            return $priorities;
        }
    }
?>